import React, { useState } from 'react';
import styles from './App.module.css';
import Header from "./components/Header";
import Menu from "./components/Menu";

function App() {
    const [menuExpand, setMenuExpand] = useState(false);

  return (
    <div>
      <Header/>
      <div className={styles.grid}>
          <Menu expand={menuExpand}/>
          <main className={styles.main}>
              <h1>Content</h1>
              <button onClick={() => setMenuExpand(!menuExpand)}>Menu toggle</button>
          </main>
      </div>
    </div>
  );
}

export default App;
