import React from 'react';
import styles from './Header.module.css'

interface HeaderProps {

}

const Header: React.FC<HeaderProps> = () => {

    return (
        <header className={styles.root}>
            Header
        </header>
    )
}

export default Header;