import React from 'react';
import cn from 'classnames';
import styles from './Menu.module.css';

interface MenuProps {
    expand?: boolean;
}

const Menu: React.FC<MenuProps> = ({ expand }) => {
    return (
        <nav className={cn(styles.root, {[styles.expand]: expand })}>
            <h2>Menu</h2>
            <ul>
                <li>Page 1</li>
                <li>Page 2</li>
            </ul>
        </nav>
    )
}

export default Menu;